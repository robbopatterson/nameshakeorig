#!/bin/bash
convert -resize 192x192 -background none icon.svg android/icon/drawable-xxxhdpi-icon.png
convert -resize 144x144 -background none icon.svg android/icon/drawable-xxhdpi-icon.png
convert -resize 96x96 -background none icon.svg android/icon/drawable-xhdpi-icon.png
convert -resize 72x72 -background none icon.svg android/icon/drawable-hdpi-icon.png
convert -resize 48x48 -background none icon.svg android/icon/drawable-mdpi-icon.png
convert -resize 36x36 -background none icon.svg android/icon/drawable-ldpi-icon.png

convert -resize 40x40 -background none icon.svg ios/icon/icon-40.png
convert -resize 80x80 -background none icon.svg ios/icon/icon-40@2x.png
convert -resize 120x120 -background none icon.svg ios/icon/icon-40@3x.png
convert -resize 50x50 -background none icon.svg ios/icon/icon-50.png
convert -resize 100x100 -background none icon.svg ios/icon/icon-50@2x.png
convert -resize 60x60 -background none icon.svg ios/icon/icon-60.png
convert -resize 120x120 -background none icon.svg ios/icon/icon-60@2x.png
convert -resize 180x180 -background none icon.svg ios/icon/icon-60@3x.png
convert -resize 72x72 -background none icon.svg ios/icon/icon-72.png
convert -resize 144x144 -background none icon.svg ios/icon/icon-72@2x.png
convert -resize 76x76 -background none icon.svg ios/icon/icon-76.png
convert -resize 152x152 -background none icon.svg ios/icon/icon-76@2x.png
convert -resize 167x167 -background none icon.svg ios/icon/icon-83.5@2x.png
convert -resize 29x29 -background none icon.svg ios/icon/icon-small.png
convert -resize 58x58 -background none icon.svg ios/icon/icon-small@2x.png
convert -resize 87x87 -background none icon.svg ios/icon/icon-small@3x.png
