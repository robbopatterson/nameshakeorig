
import { Injectable } from '@angular/core';

@Injectable() export class SwapScheduler {

    private numberSwapsPerformed = 0
    private nItems:number
    private durationMillisecs:number

    constructor(){}
    
    schedule(nItems:number, durationMillisecs:number) {
        this.nItems = nItems
        this.durationMillisecs = durationMillisecs
        this.numberSwapsPerformed = 0
    }

    isSwapRequiredNow(currentMillisecs:number) {
     
        let required = this.numberSwapsPerformed < this.expectedSwapsAsThisTime(currentMillisecs)
        console.log('isSwapRequired',required,currentMillisecs,this.numberSwapsPerformed, this.expectedSwapsAsThisTime(currentMillisecs))
        if (required)
            this.numberSwapsPerformed++
        return required
    }

    private expectedSwapsAsThisTime(currentMillisecs:number) : number {
        return this.nItems * currentMillisecs / this.durationMillisecs
    }
}