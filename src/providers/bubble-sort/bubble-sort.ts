import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

interface RankableItem{
  rank? : number
}

/*
  Generated class for the BubbleSortProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class BubbleSortProvider {

  constructor(public http: Http) {
    console.log('Hello BubbleSortProvider Provider');
  }

  nextBubbleSortIdx : number = 0;

  numBubblesRequired(items:RankableItem[]):number {
    var aCopy : RankableItem[] = items.map( item=> { return {rank:item.rank} } );
    let i = 0;
    while(this.nextBubble(aCopy)!=null) {
      i++;
    }
    return i;
  }

  /**
   * perform the next step in the bubblesort.  Return NULL if the input arrays is already sorted,
   * @param array the original array
   */
  nextBubble(array:RankableItem[], forward=false) {
    let limits = forward ?
      {start:0,continueIf:i=>(i+1<array.length),next:i=>i+1}
      : {start:array.length-2,continueIf:i=>(i>=0),next:i=>i-1}
    for(let i=limits.start; limits.continueIf(i); i=limits.next(i)) {
      let a : any = array[i]
      let b : any = array[i+1]
      if (a.rank>b.rank) {
        array[i] = b;
        array[i+1] = a;
        return array;
      }
    }
    return null; // array is totally sorted by rank
  }
}
