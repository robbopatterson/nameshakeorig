import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

export interface Person {
  name: {first:string, last:string},
  email : string, 
  picture?: {thumbnail?: string},
  rank? : number
}

/*
  Generated class for the PeopleProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PeopleProvider {

  developers : Person[] = [
    { name: {first: "Chris", last: "Brooks"}, email: "Chris.Brooks@vistavusolutions.com"  },
    { name: {first: "Chris", last: "Hogan"} , email: "Chris.Hogan@vistavusolutions.com"},
    { name: {first: "Renato", last: "Niro"} , email: "Renato.Niro@vistavusolutions.com"},
    { name: {first: "Jun", last: "Liu"}, email: "Jun.Liu@vistavusolutions.com" },
    { name: {first: "Jane", last: "Manz"}, email: "Jane.Manz@vistavusolutions.com" },
    { name: {first: "Rob", last: "Patterson"}, email: "Rob.Patterson@vistavusolutions.com" },
    { name: {first: "Puja", last: "Saha"}, email: "Puja.Saha@vistavusolutions.com" },
    { name: {first: "Tri", last: "Nguyen"}, email: "Tri.Nguyen@vistavusolutions.com" },
    { name: {first: "Austen", last: "Zheng"}, email: "Authen.Zheng@vistavusolutions.com" }  ]

  calgaryOthers : Person[] = [
    { name: {first: "Jory", last: "Lamb"}, email: "Jory.Lamb@vistavusolutions.com"  },
    { name: {first: "Luzma", last: ""} , email: "Luzma@vistavusolutions.com"},
    { name: {first: "Cecile", last: ""} , email: "Cecile@vistavusolutions.com"},
    { name: {first: "Emma", last: ""} , email: "Emma@vistavusolutions.com"},
    { name: {first: "Brian", last: ""} , email: "Brian@vistavusolutions.com"},
    { name: {first: "Louis", last: ""} , email: "Loise@vistavusolutions.com"},
    { name: {first: "Alex", last: ""} , email: "Alex@vistavusolutions.com"},
    { name: {first: "Conrad", last: ""} , email: "Conrad@vistavusolutions.com"},
    { name: {first: "Terri", last: ""} , email: "Terri@vistavusolutions.com"},
    { name: {first: "Justin", last: ""} , email: "Justin@vistavusolutions.com"},
    { name: {first: "Adam", last: ""} , email: "Adam@vistavusolutions.com"}
    
    ];

  enchanted : Person[] = [
    { name: {first: "Doc", last: "D"}, email: "Doc@enchant.com"  },
    { name: {first: "Dopey", last: "D"} , email: "Dopey@enchant.com"},
    { name: {first: "Bashful", last: "D"} , email: "Bashful@enchant.com"},
    { name: {first: "Grumpy", last: "D"}, email: "Grump@enchant.com" },
    { name: {first: "Sleepy", last: "D"}, email: "Sleepy@enchant.com" },
    { name: {first: "Sneezy", last: "D"}, email: "Sneezy@enchant.com" },
    { name: {first: "Happy", last: "D"}, email: "Happy@enchant.com" },
    { name: {first: "Smelly", last: "D"}, email: "Smelly@enchant.com" },
    { name: {first: "Feisty", last: "D"}, email: "Feisty@enchant.com" },
    { name: {first: "Burpy", last: "D"}, email: "Burpy@enchant.com" },
    { name: {first: "Snow", last: "W"}, email: "Snow@enchant.com" }
  ];

  people = []

  constructor(public http: Http) {
  }

  public getGroups() : Observable<string[]> {
    return Observable.of( ["Enchanted","Calgary","Developers","Random20"] );
  }

  public getPeople() {
    return this.people
  }

  public getRandom(nUsers:number) {
    return this.http.get(`https://randomuser.me/api/?results=${nUsers}`)
      .map(res=>res.json().results)
  }

  public get(groupName) {
    if (groupName==="Enchanted") {
      return this.enchanted;
    }
    if (groupName==="Developers") 
    {
      return this.developers;
    } else if (groupName==="Calgary") {
      return this.developers.concat(this.calgaryOthers)
    }
    return [];
  }

}
