import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { DeviceMotion } from '@ionic-native/device-motion';
import { Platform } from "ionic-angular";
import { Subscription } from "rxjs/Subscription";

/*
  Generated class for the ShakeDetectProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ShakeDetectProvider {

  constructor(public platform: Platform, private deviceMotion: DeviceMotion) {
    console.log('Hello ShakeDetectProvider Provider');
  }
  private lastX;
  private lastY;
  private lastZ;
  private moveCounter = 0;

  onAcceleration(acc, shakeDetected) {
    if (!this.lastX) {
      this.lastX = acc.x;
      this.lastY = acc.y;
      this.lastZ = acc.z;
      return;
    }

    let deltaX: number, deltaY: number, deltaZ: number;
    deltaX = Math.abs(acc.x - this.lastX);
    deltaY = Math.abs(acc.y - this.lastY);
    deltaZ = Math.abs(acc.z - this.lastZ);

    if (deltaX + deltaY + deltaZ > 7) {
      this.moveCounter++;
    } else {
      this.moveCounter = Math.max(0, --this.moveCounter);
    }

    if (this.moveCounter > 2) {
      console.log('SHAKE');
      shakeDetected();
      this.moveCounter = 0;
    }
    //console.info(this.moveCounter,deltaX,deltaY,deltaZ,acc.x,acc.y,acc.z)
    this.lastX = acc.x;
    this.lastY = acc.y;
    this.lastZ = acc.z;
  }

  private accSubscription: Subscription;
  private shakeDetected;

  public onShake(shakeDetected) {
    this.shakeDetected = shakeDetected;

    if (this.platform.is('cordova')) {
      console.info('cordova detected')
      this.platform.ready().then(() => {
        console.info('platform is ready')
        document.addEventListener("pause", ()=>this.onPause(), false);
        document.addEventListener("resume", ()=>this.onResume(), false);
        this.onResume();
      });
    }
  }

  public onResume() {
    this.accSubscription = this.deviceMotion.watchAcceleration({ frequency: 200 }).subscribe(acc => {
      this.onAcceleration(acc, this.shakeDetected)
    });
    console.log('onResume',this.accSubscription)
  }

  public onPause() {
    console.log('onPause',this.accSubscription)
    this.accSubscription.unsubscribe();
  }

}
