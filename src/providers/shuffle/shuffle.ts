import { Injectable } from '@angular/core';

/*
  Generated class for the ShuffleProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ShuffleProvider {

  constructor() {
  }

  // Returns 1 to N in a totally random order
  createShuffledRanks1ToN(n) {
    return this.shuffle( this.createRanks1ToN(n) );
  }

  // Returns 1...N
  createRanks1ToN(n) {
    let nArray = [];
    for (let i=1; i<=n; i++)
      nArray.push(i)
    return nArray  
  }

  // Fisher-Yates (aka Knuth) Shuffle. https://github.com/coolaj86/knuth-shuffle
  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

}
