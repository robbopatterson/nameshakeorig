import { Component } from '@angular/core';
import { NavController, LoadingController, Platform, NavParams } from 'ionic-angular';
import { PeopleProvider } from '../../providers/people/people';
import { ShuffleProvider } from '../../providers/shuffle/shuffle';
import { Subscription } from 'rxjs/Rx';
import {TimerObservable} from "rxjs/observable/TimerObservable";
import { BubbleSortProvider } from "../../providers/bubble-sort/bubble-sort";
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import {DeviceMotion} from '@ionic-native/device-motion';
import { ShakeDetectProvider } from "../../providers/shake-detect/shake-detect";
import {SwapScheduler} from "../../providers/bubble-sort/swap-scheduler"
//import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public people;
  readonly prizeWheelDuration : number = 3750
  public showRank = false;
  public currentGroup;

  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private peopleSvc: PeopleProvider, 
    private shuffleSvc: ShuffleProvider, 
    private loadingCtrl: LoadingController,
    private bubbleSvc: BubbleSortProvider,
    private smartAudio: SmartAudioProvider, 
    private platform:Platform,
    private deviceMotion:DeviceMotion,
    private shakeDetect:ShakeDetectProvider,
    private swapScheduler:SwapScheduler
//    private storage:Storage
    ) 
    {
    this.smartAudio.preload('sound1','./assets/sounds/prize-wheel.mp3');

    shakeDetect.onShake(() => this.randomizeOrder());
  }

  ionViewDidLoad() {
    console.log('init',this.navParams.get('init'))
    this.currentGroup = this.navParams.get('init');
    if (this.currentGroup==="Random20") {
      this.getRandom20()
    } else {
      this.people = this.peopleSvc.get(this.currentGroup);
    }
  }

  getRandom20() {
    this.peopleSvc.getRandom(20)
      .subscribe(payload => this.people = payload)
  }

  swapTwo() {
    let p0 = this.people[0];    
    this.people[0] = this.people[1];
    this.people[1] = p0;    
  }

  quicklyRandomizePeople() {
    let shuffledRanks = this.shuffleSvc.createShuffledRanks1ToN(this.people.length)
    this.people = this.people.map( 
      (person,idx) => {person.rank = shuffledRanks[idx]; return person} 
    );
    this.people = this.people.sort( (a,b) => a.rank-b.rank );
  }

  slowlyBubbleSortPeopleInNewRandomOrder(duration:number) {
    let shuffledRanks = this.shuffleSvc.createShuffledRanks1ToN(this.people.length)
    this.people = this.people.map( 
      (person,idx) => {person.rank = shuffledRanks[idx]; return person} 
    );
    let nBubbles = this.bubbleSvc.numBubblesRequired(this.people)
    this.swapScheduler.schedule(nBubbles,duration)

    if (this.timerSub==null) {
      this.showRank = false;
      this.loader = this.loadingCtrl.create({content: "Shaking it..."});
      this.loader.present();
      this.changeCount = 0;
      let timer = TimerObservable.create(0,100);
      let self = this;
      let startTime = Date.now()
      self.timerSub = timer.subscribe(t=>{
        let nextSort;
        while(self.swapScheduler.isSwapRequiredNow(Date.now()-startTime)) {
          if ( (nextSort=self.bubbleSvc.nextBubble(self.people)) != null ) {
              self.people = nextSort;
          } else if (self.timerSub!=null) {
              // We're done
              console.log('unsub')
              self.showRank = true;
              self.timerSub.unsubscribe();              
              self.timerSub = null;
              self.loader.dismiss();
          }
        }

      });
    }
  }

  private timerSub: Subscription
  private loader;
  private changeCount;
  randomizeOrder() {
    this.smartAudio.play('sound1');
    this.quicklyRandomizePeople();
    this.slowlyBubbleSortPeopleInNewRandomOrder(this.prizeWheelDuration);
  }



}
