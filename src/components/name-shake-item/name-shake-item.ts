import { Component, Input } from '@angular/core';
import { Person } from "../../providers/people/people";
import {Md5} from 'ts-md5/dist/md5';

/**
 * Generated class for the NameShakeItemComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'name-shake-item',
  templateUrl: 'name-shake-item.html'
})
export class NameShakeItemComponent {
  @Input() person : Person
  @Input() showRank : boolean

  constructor() {
  }

  gravatarUrl(person:Person, fallback:string="monsterid") {
    let hash5 = Md5.hashStr( person.email.trim().toLowerCase() )
    return `https://www.gravatar.com/avatar/${hash5}?d=${fallback}`;
  }

  thumbUrl(person:Person) {
    if (person.picture && person.picture.thumbnail)
      return person.picture.thumbnail;
    return this.gravatarUrl(person);
  }

  initials(person) {
    return (person.name.first.slice(0,1)
          + person.name.last.slice(0,1)).toUpperCase()
  }



}
