import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NameShakeItemComponent } from './name-shake-item';

@NgModule({
  declarations: [
    NameShakeItemComponent,
  ],
  imports: [
    IonicPageModule.forChild(NameShakeItemComponent),
  ],
  exports: [
    NameShakeItemComponent
  ]
})
export class NameShakeItemComponentModule {}
