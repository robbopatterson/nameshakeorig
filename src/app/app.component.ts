import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { Storage } from '@ionic/storage';
import { PeopleProvider } from "../providers/people/people";
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
//import * as firebase from 'firebase/app';
import {GooglePlus} from '@ionic-native/google-plus';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = HomePage;
  groups: any[];
  selectedGroup: string;

  constructor(
    peopleSvc: PeopleProvider,
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private storage: Storage,
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private googlePlus: GooglePlus ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      peopleSvc.getGroups().subscribe(groups => {
        this.groups = groups
        this.setGroup(groups[0]);
      });
    });
  }

  setGroup(group) {
    this.storage.set("selectedGroup", group)
    this.nav.setRoot(HomePage, { init: group })
  }

  exportGroups() {

    // Sign in with google
    // this.googlePlus.login({})
    //   .then(
    //   (res) => {
    //     console.log('good',res);
    //  //   this.userData = res;
    //   },
    //   (err) => {
    //     console.log('error');
    //     console.log(err);
    //   });

  // This version works on web
    // this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(
    //   response => {
    //     let configObservable = this.db.object('/config');
    //     configObservable.set({groups:this.groups, time: Date.now()});
    //   },
    //   error => console.error(error)
    // )

  }
}

