import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PeopleProvider } from '../providers/people/people';
import { BubbleSortProvider } from '../providers/bubble-sort/bubble-sort';
import { ShuffleProvider } from '../providers/shuffle/shuffle';
import { NativeAudio } from '@ionic-native/native-audio';
import { SmartAudioProvider } from '../providers/smart-audio/smart-audio';
import {DeviceMotion} from '@ionic-native/device-motion';
import { ShakeDetectProvider } from '../providers/shake-detect/shake-detect';
import {SwapScheduler} from "../providers/bubble-sort/swap-scheduler"
import { NameShakeItemComponent } from '../components/name-shake-item/name-shake-item';
import { IonicStorageModule } from '@ionic/storage';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFireAuthModule } from 'angularfire2/auth';
import { FIREBASE_CONFIG } from './app.firebase.config';
import {GooglePlus} from '@ionic-native/google-plus';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    NameShakeItemComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG,"name-shaker"),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PeopleProvider,
    BubbleSortProvider,
    ShuffleProvider,
    NativeAudio,
    SmartAudioProvider,
    DeviceMotion,
    ShakeDetectProvider,
    SwapScheduler,
    GooglePlus
  ]
})
export class AppModule {}
